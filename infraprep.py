#!/usr/bin/env python

import sys
import json
import yaml
import logging
import argparse
import subprocess
from jinja2 import Template
from tempfile import TemporaryFile, NamedTemporaryFile

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s: %(lineno)s %(msg)s', datefmt='%H:%M:%S %Z')
logging.getLogger().setLevel(logging.CRITICAL)

def exec_cmd(command, args=[]):
    def read_file(fd):
        fd.seek(0)
        buf = fd.read()
        return buf if len(buf) > 0 else ''
    __args = args if isinstance(args, list) else [args,]
    __command = [command,] + __args
    __stdout, __stderr = '', ''
    __rc = 1
    stderr = TemporaryFile(mode='w+')
    stdout = TemporaryFile(mode='w+')
    try:
        subprocess.check_call(__command, stderr=stderr, stdout=stdout, shell=False, universal_newlines=True)
        __rc = 0
    except subprocess.CalledProcessError as err:
        __rc = err.returncode
        logging.error("Command {0} exited with rc {1} and message '{2}'".format(" ".join(__command), __rc, err.output))
    except OSError as err:
        __stderr = err.strerror
        __rc = 1
        logging.error("Could not execute command {0} due to {1}".format(" ".join(__command), __stderr))
    finally:
        __stderr = read_file(stderr)
        __stdout = read_file(stdout)
        stderr.close()
        stdout.close()
    return (__rc, __stdout.strip(), __stderr.strip())


def validate_params(**kwargs):
    """
    Checks that the provides params are provided.
    It accepts three arguments-lists:
     - required_params - params which must be provided
     - optional_params - params which might be provided but are not a must
     - conflict_params - params which conflict with params in the optional.
                         So it checks that either params from the optional, or params
                         from the conflict list should be provided
    """
    required_params = kwargs.pop('required_params', [])
    optional_params = kwargs.pop('optional_params', [])
    conflict_params = kwargs.pop('conflict_params', [])
    assert(len(kwargs.keys()) > 0), "Probably you missed **kwargs in the validation call"
    user_params = set(kwargs.keys())
    all_valid_params = tuple(required_params) + tuple(optional_params)

    exhastive_params = set(required_params).intersection(set(optional_params))
    assert(
        len(exhastive_params) == 0
    ), "Params cannot be in the both required and optional lists. Conflicts: {0}".format(", ".join(exhastive_params))

    print(required_params, user_params)
    assert(
        set(required_params).issubset(user_params)
    ), "Missed one or more required params: {0}".format(", ".join(required_params))

    assert(
        user_params.issubset(all_valid_params)
    ), "Passed unknown param(s): {0}".format(", ".join(user_params.difference((all_valid_params))))
    return True


class CLIExec(object):
    rt_plain = str
    rt_list = list
    rt_dict = dict
    rt_json = dict
    rt_status = bool

    def run(self, command, args=[], return_type=str):
        assert(return_type in [self.rt_plain, self.rt_list, self.rt_dict, self.rt_json, self.rt_status]), "Unsupported return_type requested"
        self.__return_type = return_type
        self.__rc, self.__stdout, self.__stderr = exec_cmd(command, args)
        return self.result

    @property
    def result(self):
        if hasattr(self, '__result') and isinstance(self.__result, self.__return_type):
            return self.__result
        self.__result = None

        if self.__return_type in [self.rt_dict, self.rt_list, self.rt_json]:
            self.__result = json.loads(self.__stdout)
        elif self.__return_type == self.rt_status:
            self.__result = (self.__rc == 0)
        elif self.__return_type == self.rt_plain:
            self.__result = self.__stdout
        else:
            logging.critical("You should never see this message")
        if self.__stderr != '':
            logging.debug(self.__stderr)
        assert(self.__result != None), "Unexpected result (None)"
        return self.__result

    @property
    def stdout(self):
        return self.__stdout

    @property
    def stderr(self):
        return self.__stderr

    @property
    def success(self):
        return (self.__rc == 0)


def parse_json(fd):
    result = None
    try:
        result = json.load(fd)
    except ValueError as err:
        logging.critical("JSON file is not valid. Check your config file. " + err.message)
        exit(1)
    return result


class ConfigData(object):
    def __init__(self, fd):
        self.__json = parse_json(fd)

    def validate(self, path_items):
        assert(isinstance(path_items in [dict, list])), "Should be a list or a tuple but given: " + type(path_items)

    def data(self, json_path=''):
        """
        returns data which located at json_path.
        json_path can be just a single object or
        a sequence of objects like object.object.object
        """
        elems = json_path.split('.')
        if json_path.strip() == '':
            result = self.__json
        elif not isinstance(self.__json, dict):
            logging.error("Expect JSON structure but it is not")
            exit(1)
        elif isinstance(elems, list):
            result = self.__json if len(elems) == 0 else self.__json[elems[0]]
        if len(elems) > 1:
            for elem in elems[1:]:
                result = result[elem]
        return result

YAML_TEMPLATES = {
    "ConfigMap": """
apiVersion: v1
kind: ConfigMap
metadata:
    name: {name}
    labels:
        app: {name}
data:
""",
    "Route": """
apiVersion: v1
kind: Route
metadata:
    name: {name}
    labels:
        app: {name}
spec:
    to:
        kind: Service
        name: {name}
""",
    "Service": """
apiVersion: v1
kind: Service
metadata:
    name: {name}
    labels:
        app: {name}
spec:
    selector:
        app: {name}
    ports:
""",
    "DeploymentConfig": """
apiVersion: apps.openshift.io/v1
kind: DeploymentConfig
metadata:
    name: {name}
    labels:
        app: {name}
spec:
    replicas: 1
    selector:
        app: {name}
    strategy:
        type: Rolling
    template:
        metadata:
            name: {name}
            labels:
                app: {name}
        spec:
            containers:
            - name: {name}
              image: >-
                {image_name}
    triggers:
      - type: ConfigChange
"""
}

def get_image_ports(image_url, oc_path):
    """Returns a list of tuples described exposed ports"""
    image = CLIExec()
    raw_metadata = image.run(oc_path,
        ['import-image', '--dry-run', 'testname', '--insecure', '--confirm', '--from', image_url],
        return_type=image.rt_plain
    )
    if not image.success:
        logging.critical("Could not get metadata about the image: {0}".format(image.stderr))
        return None

    ports = []
    for line in raw_metadata.split('\n'):
        if line.strip().startswith('Exposes Ports:'):
            str_ports = line.split(':', 1)[1].strip().split(',')
            print(str_ports)
            for p in str_ports:
                ports.append(
                    p.split('/')
                )
            logging.debug("Ports exposed by the image: {0}".format(ports))
            break
    else:
        logging.warning("The image {0} does not expose ports".format(image_url))
    return ports


def resource_schema(**kwargs):
    """
    Creates a YAML config from a template
    Params:
        - content
        - variables to substitute them in the content
    """
    """
    validate_params(
        required_params=('content',),
        optional_params=('env', 'name', 'replicas', 'ports'),
        **kwargs
    )
    """
    return yaml.load(kwargs['content'].format(**kwargs))
    # t = Template(kwargs['content'])
    # return t.render(**kwargs).strip()


class OCClient(CLIExec):
    def __init__(self, oc_client_path):
        self.__command = oc_client_path
    def run(self, args=[], return_type=CLIExec.rt_plain):
        # Python 2
        return super(OCClient, self).run(command=self.__command, args=args, return_type=return_type)

class OpenShiftResource(object):
    def __init__(self, oc_client_path):
        self.__oc_client_path = oc_client_path
        self.__oc_client = OCClient(oc_client_path)

    @property
    def oc(self):
        return self.__oc_client

    def render(self, **kwargs):
        """
        Creates openshift resources.
        Params:
            - resources - a list of OpenShift resource names to create.
                          That should be the names as they defined in
                          YAML_TEMPLATES
            **vars - variables to pass to the templates
        """
        validate_params(
            required_params=('name', 'resources'),
            optional_params=('ports', 'labels', 'image_name', 'env'),
            **kwargs
        )
        requested_resources = kwargs['resources']
        assert(isinstance(requested_resources, list)), "Resources must be a list"

        resources = list()
        for resource in requested_resources:
            resources.append(resource_schema(content=YAML_TEMPLATES[resource], **kwargs))
        ports = get_image_ports(oc_path='oc', image_url=kwargs['image_name'])


        # Update Service, Router and DC with ports information
        if isinstance(ports, list) and len(ports) > 0:
            service = next((r for r in resources if r['kind'] == 'Service' and r['metadata']['name'] == kwargs['name']), None)
            if service:
                schema_ports = list()
                for p in ports:
                    schema_ports.append({
                        "name": "port_" + p[0],
                        "targetPort": p[0],
                        "port": p[0],
                        "protocol": p[1].upper()
                    })
                service['spec']['ports'] = schema_ports
                logging.info("Patched the Service resource with ports data")
            for r in resources:
                if r['kind'] == 'Router' and service is not None:
                    # implement adding ports here
                    pass
                elif r['kind'] == 'Router' and service is None:
                    resource.remove(r)
                    logging.info("Resource Router removed because there are no ports")
        if kwargs.get('env', None) is not None:
            cm = next((r for r in resources if r['kind'] == 'ConfigMap' and r['metadata']['name'] == kwargs['name']), None)
            cm['data'] = kwargs['env']
            logging.info("Patched the Config Map with the environment variables")
            dc = next((r for r in resources if r['kind'] == 'DeploymentConfig' and r['metadata']['name'] == kwargs['name']), None)
            if dc:
                envs = []
                for env in kwargs['env'].keys():
                    envs.append({
                        "name": env,
                        "valueFrom": {
                            "configMapKeyRef": {
                                "name": kwargs['name'],
                                "key": env,
                                "value": kwargs['env'][env]
                            }
                        }
                    }ear

                else:
                    dc['spec']['template']['spec']['containers'][0]['env'] = envs
                    logging.info("Patched the Deployment Config with the environment variables")
        return resources

    def create(self, **kwargs):
        """
        Creates resources from the provided yam schemas.
        Params:
            @schemas - a list of yaml schemas (resources) to create
        """
        validate_params(
            required_params=('schemas',),
            **kwargs
        )
        assert(isinstance(kwargs['schemas'], list)), "Argument schemas must be a list of dicts"
        buf_file = NamedTemporaryFile(delete=False)
        buf_file.write(yaml.dump(kwargs['schemas']))
        buf_file.close()
        print("Saved as:", buf_file.name)
        pass

    def get(self, **kwargs):
        """
        Request details about one or more resources.
        Params:
            - resources - a list of tuples (kind, name) of resources to request
            - yaml - a yaml buffer with the resources to request
            It is enough to use only parameter but if two are given,
            the resources takes a priority
            It always returns True (if success) or False otherwise
        """
        # validate_params(optional_params=('name', 'kind', 'yaml'), **kwargs)
        validate_params(optional_params=('resources', 'yaml'), **kwargs)
        list_resources = kwargs.get('resources', [])
        yaml = kwargs.get('yaml', None)
        rt_type = kwargs.get('rt_type', CLIExec.rt_status)
        resources = None
        if None not in [resources, ]:
            resources = ["{0}/{1}".format(kind, name) for kind, name in list_resources]
        elif None not in [yaml,]:
            if isinstance(yaml, list):
                resources = ["{0}/{1}".format(y['kind'], y['metadata']['name']) for y in yaml]
            elif isinstance(yaml, [y for y in yaml]):
                resources = ["{0}/{1}".format(yaml['kind'], yaml['metadata']['name']),]
        logging.info("Get details of {0}".format(", ".join(resources)))
        assert(resource != None), "Params kind and name or yaml should be used"
        return self.__oc_client.run(['get', resource, '--output', 'json'], return_type=rt_type)

    def create2(self, **kwargs):
        pass
    pass


def dev_funcs():
    oc = OpenShiftResource('oc')
    envs = {
        'HTTP_PROXY': 'myproxy.example.com:8080',
        'HTTPS_PROXY': 'myproxy.example.com:8080'
    }
    schemas = oc.render(resources=['DeploymentConfig', 'ConfigMap', 'Service', 'Route'], name='myservice', env=envs, image_name='docker.io/nginx:latest')
    oc.create(schemas=schemas)
    pass

def main():
    parser = argparse.ArgumentParser(description='Boarding OpenShift projects')
    parser.add_argument('-a', '--action', choices=('validate', 'execute', 'dev'), required=True,
                        help='Configuration file in JSON format')
    parser.add_argument('-c', '--config-file', type=argparse.FileType('r'),
                        required=True, default=sys.stdin, help='Configuration file in JSON format')
    parser.add_argument('-e', '--exe', help='Path to the OC CLI command. If not provided a shell should be able to find it')
    parser.add_argument('--log-level', default='critical', choices=('info', 'debug', 'warning', 'error', 'critical'), help='Set logging verbosity (default: info)')
    parser.add_argument('--wait-complete', action='store_true', help='Wait until all resources will be run successfully')
    args = parser.parse_args()

    log_levels = { 'info': logging.INFO, 'debug': logging.DEBUG, 'warning': logging.WARNING, 'error': logging.ERROR, 'critical': logging.CRITICAL }
    logging.getLogger().setLevel(log_levels[args.log_level])

    if args.action == 'validate':
        parse_json(args.config_file)
    elif args.action == 'execute':
        c = ConfigData(args.config_file)
        logging.info(c.data('first_name'))
        logging.info(c.data('colors.mixed'))
        logging.info(c.data('colors.mixed.last'))
        if 'col' not in c.data().keys():
            logging.error('Key col is not in data')
    elif args.action == 'dev':
        dev_funcs()

if __name__ == "__main__":
   main()
