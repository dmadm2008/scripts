#!/usr/bin/env python

import os
import sys
import json
import yaml
import logging
from urlparse import urljoin
from requests import Session, HTTPError


class ConfluenceClient(object):
    def __init__(self, url, username, password):
        self.url, self.username, self.password = url.rstrip('/'), username, password
        self.session = Session()
        self.session.auth = (username, password)
        self.session.headers.update({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })


    def _make_call(self, url, method, action_msg, json=None, data=None, response_format='response', params={}, files={}, headers={}):
        """
        Makes a call to Confluence
        :param url:
        :param method:
        :param json: OPTIONAL
        :param response_format: Supported 'response', 'json'
        """
        if response_format not in [ "response", "json" ]:
	        raise ValueError("Unsupported value")

        logging.debug("Making HTTP request {msg} ({method}:{url})".format(method=method, url=url, msg=action_msg.upper()))
        response = None
        try:
             response = self.session.request(url=self.url+url, method=method, json=json, params=params, data=data,
                                            files=files, headers=headers)
             logging.info("Request {} completed with code {}".format(action_msg.upper(), response.status_code))
        except HTTPError as e:
            if e.response.status_code == 404:
                logging.error("The calling user does not have permissions for {}".format(action_msg.upper()))
            raise
        return response.json() if (response_format == 'json') else response


    @staticmethod
    def _create_body(body, representation):
        if representation not in ['editor', 'export_view', 'view', 'storage', 'wiki']:
            raise ValueError("Wrong value for representation, it should be either wiki or storage")

        return {
            representation: {
                'value': body,
                'representation': representation}
        }


    def create_page(self, space, title, content=None, parent_id=None,
                    type='page', representation='storage', editor=None):
        """
        Creates a new page
        :param space:
        :param title:
        :param content:
        :param parent_id:
        :param type:
        :param representation: OPTIONAL: either Confluence 'storage' or 'wiki' markup format
        :param editor: OPTIONAL: v2 to be created in the new editor
        """
        if representation not in [ "editor", "export_view", "view", "storage", "wiki" ]:
            raise ValueError("Wrong value for representation, it should be either wiki or storage")
        logging.info('Creating {type} "{space}" -> "{title}"'.format(space=space, title=title, type=type))
        url = '/rest/api/content'
        data = {
            "type": type,
            "title": title,
            "space": {"key": space},
            "body": self._create_body(content, representation)
        }
        if parent_id:
            data["ancestores"] = [{"type": type, "id": parent_id}]
        if editor == "v2":
            data["metadata"] = {"properties": {"editor": {"value": "v2"}}}
        return self._make_call(url, "POST", "CREATE PAGE", json=data, response_format="json")


    def update_page(self, page_id, content=None, parent_id=None, type='page', representation='wiki',
                    minor_edit=False, version_component=None):
        pass


    def delete_attachment(self, page_id, filename, version=None):
        params = {"pageId", page_id, "fileName": filename}
        if version:
            params["version"] = version
        return self._make_call(url="json/removeattachment.action", method="POST", params=params)


    def attach_file(self, page_id, filename, name=None, content_type=None, comment=None):
        if name is None:
            name = os.path.basename(filename)
        if content_type is None:
            content_type = "application/octet-stream"
        content = open(filename, "rb").read()
        data = {
            "type": "attachment",
            "fileName": filename,
            "contentType": content_type,
            "comment": comment or "Uploaded {filename}".format(filename=filename),
            "minorEdit": 'true',
        }
        headers = {
            "X-Atlassian-Token": "nocheck",
            "Accept": "application/json",
        }
        path = "/rest/api/content/{page_id}/child/attachment".format(page_id=page_id)
        attachments = self._make_call(url=path, method="GET", action_msg="GET ATTACHMENT", headers=headers,
                                      params={"filename": filename}, response_format="json")
        if attachments.get("size"):
            path = "{path}/{att_id}/data".format(path=path, att_id=attachments["results"][0]["id"])
        response = self._make_call(url=path, method="POST", action_msg="UPLOAD ATTACHMENT", data=data, headers=headers,
                                   files={"file": (filename, content, content_type)}, response_format='response')
        print(response.text, response.headers)
        print(data)
        return response


    def delete_page_by_id(self, page_id):
        url = "/rest/api/content/{page_id}".format(page_id=page_id)
        response = self._make_call(url, "DELETE", "DELETE PAGE BY ID", json={}, response_format='response')
        r_json = response.json()
        if response.status_code == 200 and r_json["successful"]:
            logging.info("Page id {} has been deleted".format(page_id))
        else:
            logging.warn("Page id {} has not been deleted: {}".format(page_id, r_json["message"]))
        return response

    def update_page(self, page_id, content, representation="wiki"):
        pass

    def upload_attachment(self, page_id, filepath):
        pass

    def download_attachment(self, page_id, attachment_name):
        pass

def main_cli():
    import argparse

    logging.basicConfig(format="%(asctime)s :: %(levelname)s :: %(msg)s", level=logging.DEBUG)
    actions = ( "create-page", "delete-page-by-title", "delete-page-by-id", "upload-file", "delete-attachment" )

    parser = argparse.ArgumentParser(description="Confluence Client",
                                     epilog="Set CONFLUENCE_USER and CONFLUENCE_PASS environment variables as credentials")
    parser.add_argument("-a", "--action", choices=actions, required=True, help="Specify an action to perform")
    parser.add_argument("--url", required=True, help="Confluence API endpoint")
    parser.add_argument("-s", "--space", required=False, help="Confluence space")
    parser.add_argument("-t", "--title", required=False, help="Page title")
    parser.add_argument("-c", "--content", required=False, type=argparse.FileType("r"), help="File with content for the page")
    parser.add_argument("-p", "--parent-id", required=False, help="Id of a parent page, if needed")
    parser.add_argument("-f", "--format", choices=("wiki", "storage"), default="wiki", help="Content format. Default 'wiki'")
    parser.add_argument("-g", "--page-id", required=False, help="Id of a page to delete")
    parser.add_argument("-u", "--upload-file", required=False, type=argparse.FileType("r"), help="Attach file to a page")

    username, password = os.environ.get("CONFLUENCE_USER"), os.environ.get("CONFLUENCE_PASS")

    args = parser.parse_args()

    if args.action in actions:
        cf_client = ConfluenceClient(args.url, username, password)
        if args.action == "create-page":
            if not all([ args.space, args.title, args.content ]):
                parser.error("This action requires: -s/--space, -t/--title, -c/--content")
            cf_client.create_page(args.space, args.title, parent_id=args.parent_id, representation=args.format, content=args.content.read())
        elif args.action == "delete-page-by-id":
            if not all([ args.page_id ]):
                parser.error("This action requires: -g/--page-id")
            cf_client.delete_page_by_id(args.page_id)
        elif args.action == "upload-file":
            if not all([args.upload_file, args.page_id]):
                parser.error("This action requires: -g/--page-id, -u/--upload-file")
            cf_client.attach_file(page_id=args.page_id, filename=args.upload_file.name)
        elif args.action == "delete-attachment":
            pass



def main_cli_2():
    import argparse

    logging.basicConfig(format="%(asctime)s :: %(levelname)s :: %(msg)s", level=logging.DEBUG)

    parser = argparse.ArgumentParser(description="Confluence Client", add_help=False,
                                     epilog="Set CONFLUENCE_USER and CONFLUENCE_PASS environment variables as credentials")
    # parser.add_argument("--url", required=True, help="Confluence API endpoint")
    subparsers = parser.add_subparsers(title="actions", dest="subcmd", help="sub-command help")

    parser_create_page = subparsers.add_parser("create-page", parents=[parser], help="create-page help")
    parser_create_page.add_argument("--url", required=True, help="Confluence API endpoint")
    parser_create_page.add_argument("-s", "--space", required=True, help="Confluence space")
    parser_create_page.add_argument("-t", "--title", required=True, help="Page title")
    parser_create_page.add_argument("-c", "--content", required=True, type=argparse.FileType("r"), help="File with content for the page")
    parser_create_page.add_argument("-p", "--parent-id", required=False, help="Id of a parent page, if needed")
    parser_create_page.add_argument("-f", "--format", choices=("wiki", "storage"), default="wiki", help="Content format. Default 'wiki'")

    parser_delete_page = subparsers.add_parser("delete-page", parents=[parser], help="delete-page help")
    parser_delete_page.add_argument("--url", required=False, help="Confluence API endpoint")
    parser_delete_page.add_argument("-s", "--space", required=False, help="Confluence space where to perform search and delete")
    parser_delete_page.add_argument("-t", "--title", required=False, help="Search and delete the page with the title")
    parser_delete_page.add_argument("-p", "--page-id", required=False, help="Id of a page to delete")

    username, password = os.environ.get("CONFLUENCE_USER"), os.environ.get("CONFLUENCE_PASS")

    args = parser.parse_args()
    cf_client = None
    if args.subcmd in ( "create-page", "delete-page" ):
        cf_client = ConfluenceClient(args.url, username, password)

    if args.subcmd == 'create-page':
        cf_client.create_page(args.space, args.title, parent_id=args.parent_id, representation=args.format, content=args.content.read())

    elif args.subcmd == 'delete-page':
        if args.page_id is not None:
            cf_client.delete_page_by_id(args.page_id)
        else:
            if args.space is None or args.title is None:
                parser.error("Must be provided -p/--page-id or -s/--space with -t/--title")
            cf_client.delete_page_by_title(args.space, args.title)


if __name__ == "__main__":
    main_cli()

