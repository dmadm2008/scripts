#!/usr/bin/env python

import unittest
from infraprep import exec_cmd, CLIExec, validate_params

class TestSuite1(unittest.TestCase):
    def test_1(self):
        rc, stdout, stderr = exec_cmd('df')
        self.assertEqual(rc, 0), "Result should be True"
    def test_2(self):
        rc, stdout, stderr = exec_cmd('echo', 'Super')
        self.assertEqual(rc, 0), "Result should be True"
        self.assertEqual(stdout, 'Super'), "Result should be Super"
        self.assertEqual(stderr, ''), "Stderr should be empty"
    def test_3(self):
        rc, stdout, stderr = exec_cmd('false')
        self.assertNotEqual(rc, 0), "Result should not be True"
        self.assertEqual(stdout, ''), "Output should be empty"
    def test_4(self):
        rc, stdout, stderr = exec_cmd('/bin/fakecommand')
        self.assertNotEqual(rc, 0), "Result should not be True"
        self.assertEqual(stdout, ''), "Output should be empty"
    def test_5(self):
        rc, stdout, stderr = exec_cmd('echo', ['Super', 'baby'])
        self.assertEqual(rc, 0), "Result should be True"
        self.assertEqual(stdout, 'Super baby'), "Result should be Super baby"
        self.assertEqual(stderr, ''), "Stderr should be empty"

class TestSuite2(unittest.TestCase):
    def test_1(self):
        c = CLIExec()
        result = c.run('echo', ['{"first_name":"John","last_name":"Snow","house":"Starks"}'], return_type=c.rt_json)
        self.assertEqual(c.stdout, '{"first_name":"John","last_name":"Snow","house":"Starks"}'), "Output should be equal"
        self.assertEqual(result['first_name'], "John"), "Output should be John"
        self.assertEqual(c.result['first_name'], "John"), "Output should be John"

    def test_2(self):
        c = CLIExec()
        result = c.run('echo', ['[1, 2, 3, 4, 5]'], return_type=c.rt_json)
        self.assertEqual(set(result), set([1,2,3,4,5])), "Output should be equal"

    def test_3(self):
        c = CLIExec()
        result = c.run('touch', ['/tmp/test1_file', '/tmp/test2_file'], return_type=c.rt_status)
        self.assertTrue(result), "Files should be created"
        result = c.run('ls', ['-1', '/tmp/test1_file'], return_type=c.rt_plain)
        self.assertEqual(result, '/tmp/test1_file'), "Should print a name of file"
        result = c.run('rm', ['-f', '/tmp/test1_file', '/tmp/test2_file'], return_type=c.rt_status)
        self.assertTrue(result), "Files should be removed"
        self.assertTrue(c.success), "Files should be removed"
        result = c.run('ls', ['-1', '/tmp/test1_file', '/tmp/test2_file'], return_type=c.rt_status)
        self.assertFalse(result), "Files should not exist"
        self.assertFalse(c.success), "It should exit with non zero"
        result = c.run('ls', ['-1', '/tmp/test2_file'])
        self.assertTrue(c.stderr.endswith('No such file or directory')), "Stderr should end with 'No such file or directory'"

    def test_4(self):
        c = CLIExec()
        result = c.run('echo', ['{"first_name":"John","last_name":"Snow","house":"Starks"}'], return_type=c.rt_json)
        self.assertTrue(result), "Should be True"


class TestSuite3(unittest.TestCase):
    def test_1(self):
        with self.assertRaises(AssertionError):
            validate_params(required_params=('one', 'two'))

        with self.assertRaises(AssertionError):
            validate_params(required_params=('one', 'two', 'three'), one=1, two=2, three=3, four=4)

        with self.assertRaises(AssertionError):
            validate_params(required_params=('one', 'two'), one=1, three=3)

        with self.assertRaises(AssertionError):
            validate_params(required_params=('one', 'two', 'four'), one=1, three=3)

        with self.assertRaises(AssertionError):
            validate_params(required_params=('one', 'two', 'four'), one=1, two=2, three=3, four=4)

    def test_2(self):
        self.assertTrue(validate_params(
            required_params=('two',),
            optional_params=('one', 'three'),
            one=1, two=2)
        ), "Only valid params should be passed"

    def test_3(self):
        self.assertTrue(validate_params(
            required_params=('two',),
            optional_params=('one', 'three'),
            one=1, two=2, three=3)
        ), "Only valid params should be passed"

    def test_4(self):
        self.assertTrue(validate_params(
            required_params=('two', 'five'),
            optional_params=('one', 'three'),
            one=1, two=2, five=5)
        ), "Only valid params should be passed"



if __name__ == "__main__":
    unittest.main()
