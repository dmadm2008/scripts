#!/usr/bin/env python
#
# Run
# $ uvicorn fileserver:app --reload --host 0.0.0.0
#
# Upload file:
# curl -F 'file=@source.code' http://nuc10.airlan.local:8000/files/am/eng2
#
# Download file:
# curl http://nuc10.airlan.local:8000/files/am/eng2/source.code
#
# Delete file:
# curl -X DELETE http://nuc10.airlan.local:8000/files/am/eng2/source.code
#
# Prefix /files must always exist, the rest part is a directory inside a server
# you can set any
#

import os
import logging
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import FileResponse

logging.basicConfig(format="%(asctime)s :: %(levelname)s :: %(message)s", level=logging.INFO)

app = FastAPI()

script_dir = os.path.dirname(os.path.abspath(__file__))
upload_dir = os.path.join( script_dir, "uploaded_files" )

logging.info("Upload directory: {}".format(upload_dir))


if not os.path.exists(upload_dir):
    os.makedirs(upload_dir)
    logging.info("Created upload directory: {}".format(upload_dir))


def make_dirs(path):
    try:
        os.makedirs(path)
        logging.info("Created directory {}".format(path))
    except FileExistsError:
        logging.debug("Directory {} already exists".format(path))
    return True


@app.get("/files/{file_path:path}")
async def download_file(file_path: str):
    while file_path.startswith("/"):
        file_path = file_path[1:]
    real_path = os.path.join(upload_dir, file_path)
    if os.path.exists(real_path):
        return FileResponse(real_path)
    else:
        raise HTTPException(status_code=404)


@app.delete("/files/{file_path:path}")
async def delete_file(file_path: str):
    while file_path.startswith("/"):
        file_path = file_path[1:]
    real_path = os.path.join(upload_dir, file_path)
    try:
        os.remove(real_path)
        logging.info("File {} deleted".format(file_path))
    except FileNotFoundError:
        logging.info("File {} does not exist".format(file_path))
    return {"filename": file_path, "action": "delete", "success": True}


@app.post("/files/{file_path:path}")
async def upload_file(file_path: str, file: UploadFile = File(...)):
    rel_path = file_path
    abs_path = os.path.join(upload_dir, rel_path)
    if not rel_path.startswith("/"):
        rel_path = "/" + rel_path
    make_dirs(abs_path)
    dest_file = os.path.join( abs_path, file.filename )
    if os.path.exists(dest_file):
        logging.info("File {} will be overwritten".format(dest_file))
    with open(dest_file, "wb") as fd:
        fd.write(await file.read())
        logging.info("Uploaded file: {}".format(dest_file))
        return {"filename": file.filename, "action": "upload", "success": True, "destination": rel_path}
    raise HTTPException(status_code=500, filename=file.filename, action="upload", success=False)

