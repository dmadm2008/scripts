#!/usr/bin/env python

from build_project import *
import unittest


class Test(unittest.TestCase):
    projects = (
        'testcase_1',
        'testcase_2',
        'testcase_3',
        'testcase_4',
        'testcase_5'
    )
    def test_1(self):
        args = {
            'server': 'https://openshift.42c60972c7d845bf9f6a.eastus2.azmosa.io',
            'token': 'qFFQ5-WD-w3lkOe1Posp5CoGWUCqZn8pIvpxI6DgY-A',
        }
        for project in self.projects:
            args.update({'project': project})
            r = RESTClient(**args)
            print("Create project: {0}".format(project))
            self.assertTrue(r.create_project()), "Create project should return True"

    def disabled_test_2(self):
        args = {
            'server': 'https://openshift.42c60972c7d845bf9f6a.eastus2.azmosa.io',
            'token': 'qFFQ5-WD-w3lkOe1Posp5CoGWUCqZn8pIvpxI6DgY-A',
        }
        for project in self.projects:
            args.update({'project': project})
            r = RESTClient(**args)
            print("Delete project: {0}".format(project))
            self.assertTrue(r.delete_project()), "Delete project should return True"


if __name__ == "__main__":
    unittest.main()
