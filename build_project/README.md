# Build Project

Deploy resources.


## Examples

    python build_project.py -a devel -s https://openshift.xxx.azmosa.io/ -t 'gsVrQzqU-1ZCKteV6o9j-5k3e5MuJ30U-k8e_D7bPrw' --log-level debug -p test4 --app-name test3app --services='[{"port":"8443","host":"super.com"},{"port":"7554","host":"google.com"},{"port":"8800","host":"aws.com"}]'  --log-level info --env-vars '{"CERT_PATH":"/certs", "PRIVATE_KEY":"private.key", "CERTIFICATE":"server.crt"}' --attach-certs '{"mount_to": "/certs", "files": ["private.key", "server.crt"]}' --log-level info --delete-project --image-name docker.io/nginx:1.17

Files `private.key` and `server.crt` should exist next to the script.

