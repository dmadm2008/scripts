#!/usr/bin/env python

import os
import re
import json
import copy
import argparse
import requests
import logging
from time import sleep
from collections import namedtuple


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s: %(msg)s', datefmt='%H:%M:%S %Z')
logging.getLogger().setLevel(logging.CRITICAL)


TEMPLATE = {
    'kind': 'List',
    'apiVersion': 'v1',
    'metadata': {
        'name': '${APP_NAME}',
        'labels': {}
    },
    'message': 'Application generated from the template',
    'items': [
        {
            'kind': 'Service',
            'apiVersion': 'v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {'app': '${APP_NAME}'}
            },
            'spec': {
                'selector': {
                    'app': '${APP_NAME}'
                },
                'ports': []
            }
        },
        {
            'kind': 'Secret',
            'apiVersion': 'v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {'app': '${APP_NAME}'}
            },
            'type': 'Opaque',
            'data': {}
        },
        {
            'kind': 'Route',
            'apiVersion': 'route.openshift.io/v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {'app': '${APP_NAME}'}
            },
            'spec': {
                'to': {
                    'kind': 'Service',
                    'name': '${APP_NAME}'
                },
                'port': {
                    'targetPort': 'svc${SERVICE_PORT}'
                },
                'tls': {
                    'termination': 'edge'
                }
            }
        },
        {
            'kind': 'ConfigMap',
            'apiVersion': 'v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {}
            },
            'data': {}
        },
        {
            'kind': 'ServiceAccount',
            'apiVersion': 'v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {'app': '${APP_NAME}'}
            },
        },
        {
            'kind': 'DeploymentConfig',
            'apiVersion': 'apps.openshift.io/v1',
            'metadata': {
                'name': '${APP_NAME}',
                'labels': {'app': '${APP_NAME}'}
            },
            'spec': {
                'replicas': '1',
                'selector': {
                    'app': '${APP_NAME}'
                },
                'template': {
                    'metadata': {
                        'name': '${APP_NAME}',
                        'labels': {'app': '${APP_NAME}'}
                    },
                    'spec': {
                        'volumes': [],
                        'containers': [{
                            'name': '${APP_NAME}',
                            'image': '${IMAGE_NAME}',
                            'ports': [],
                            'volumeMounts': [],
                            'env': {},
                            'envFrom': [{"configMapRef": {"name":"${APP_NAME}"}}]
                        }]
                    }
                }
            }
        }
    ]
}

def validate_params(**kwargs):
    """validates that the only expected parameters are passed"""
    required_params = kwargs.pop('required_params', ())
    optional_params = kwargs.pop('optional_params', ())
    missed_required = []
    for key in required_params:
        if key not in kwargs.keys():
            missed_required.append(key)
    missed_optional = []
    for key in missed_optional:
        if key not in kwargs.keys():
            missed_optional.append(key)
    unexpected_params = []
    for key in kwargs.keys():
        if key not in required_params + optional_params:
            unexpected_params.append(key)
    assert(len(missed_required) == 0), "Missed required params: " + ", ".join(missed_required)
    assert(len(missed_optional) == 0), "Missed optional params: " + ", ".join(missed_optional)
    assert(len(unexpected_params) == 0), "Unexpected params: " + ", ".join(unexpected_params)


def apicall(**kwargs):
    """
    Make an API call. Accepted parameters
    - method - possible values: GET, PUT
    """
    validate_params(
        required_params=('method', 'server', 'url'),
        optional_params=('ignore_ssl_errors',),
        **kwargs
    )
    response = requests
    pass


#def populate_ports(template, services, env_vars, attach_certs, attach_files):
def populate_ports(**kwargs):
    """
    takes services and populates ports information in it
    to routes, service, and deploymentconfig. Services should have
    this structure:
    hostname,and protocol are optional parameters. If not provided, OpenShift assigns
        hostname itself, protocol takes TCP by default
    """
    template = kwargs['template']
    resources_list = []
    services = kwargs['services']
    attach_certs = kwargs.get('attach_certs', [])
    env_vars = kwargs.get('env_vars', {})
    # schema_routes, schema_service, schema_cm, schema_dc = [], {}, {}, {}
    schema_secrets = []
    for resource in template['items']:
        if resource['kind'] == 'Service':
            ports = []
            r_service = resource
            for service in services:
                ports.append({
                    'name': 'port-{0}'.format(service['port']),
                    'port': service['port'],
                    'protocol': service.get('protocol', 'TCP').upper(),
                    'targetPort': 'port-' + service['port']
                })
            r_service['spec']['ports'] = ports
            resources_list.append(r_service)
        elif resource['kind'] == 'Secret':
            secret = copy.deepcopy(resource)
            # processing certs, if --attach-certs is given
            for f in attach_certs.get('files', []):
                if os.path.exists(f):
                    secret['data'].update({f: open(f, 'r').read().strip()})
                else:
                    logging.warning("Given cert file '{0}' wasn't found".format(f))
            resources_list.append(secret)
        elif resource['kind'] == 'Route':
            schema_routes = []
            for index, service in enumerate(services, start=1):
                route = copy.deepcopy(resource)
                route.update({
                    'metadata': {'name': '${APP_NAME}-' + str(index), 'labels': {}},
                    'spec': {
                        'host': service.get('host', ''),
                        'port': {'targetPort': 'port-{0}'.format(service['port'])},
                        'tls': {'termination': 'passthrough'},
                        'to': {'name': '${APP_NAME}', 'kind': 'Service'}
                    }
                })
                schema_routes.append(route)
            resources_list += schema_routes
        elif resource['kind'] == 'DeploymentConfig':
            deployment = resource
            containers = deployment['spec']['template']['spec']['containers']
            deployment['spec']['template']['spec']['volumes'].append({
                'name': 'certs',
                'secret': {'secretName': '${APP_NAME}'}
            })

            for container in containers:
                for service in services:
                    container['ports'].append({
                        'name': 'port-{0}'.format(service['port']),
                        'containerPort': service['port'],
                        'port': service['port']
                    })
                if len(attach_certs.get('files', [])) > 0:
                    container['volumeMounts'].append({
                        'mountPath': attach_certs['mount_to'],
                        'name': 'certs'
                    })
            deployment['spec']['template']['spec']['containers'] = containers
            resources_list.append(deployment)
        elif resource['kind'] == 'ConfigMap':
            cm = resource
            for name, value in env_vars.items():
                cm['data'].update({name: value})
            template['items'].remove(resource)
            resources_list.append(cm)
        else:
            resources_list.append(resource)
    template['items'] = resources_list
    return template

class RESTClient(object):
    api_prefixes = (
        ('apis', (
            'DeploymentConfig',
            'Route',
        )),
        ('api', (
            'Service',
            'ConfigMap',
            'ServiceAccount',
            'Secret'
        ))
    )
    def __init__(self, **kwargs):
        validate_params(
            required_params=('server', 'token', 'project', 'app_name',),
            optional_params=('http_proxy', 'https_proxy', 'timeout', 'services', 'delete_project',
                'env_vars', 'attach_certs', 'attach_files', 'image_name'),
            **kwargs
        )
        self.__session = requests.Session()
        self.__params = kwargs
        self.__session.headers.update({'Authorization': 'Bearer {0}'.format(kwargs['token'])})
        while kwargs['server'][-1] == '/':
            kwargs['server'] = kwargs['server'][:-1]
        self.p = namedtuple('Params', kwargs.keys())(*kwargs.values())


    def install_resources(self, **kwargs):
        resources = self.process_template(**kwargs)['items']
        logging.debug("Processing resources: {0}".format(resources))
        failed_resources = []
        api_prefix = lambda kind: [apiver for apiver, kinds in self.api_prefixes if kind in kinds][0]
        failed_resources = []
        for resource in resources:
            kind = resource['kind']
            name = resource['metadata']['name']
            api_version = resource['apiVersion']
            uri='/{api_prefix}/{api_version}/namespaces/{project}/{kind}s'.format(
                api_prefix=api_prefix(kind), api_version=api_version, kind=kind.lower(),
                project=self.p.project)
            response = self.status_resource(uri=uri, name=name)
            # if a resource exists
            # then we need to update/patch it
            if response.status_code in [200, 201]:
                old_resource = response.json()
                metadata = old_resource['metadata']
                resource.update({
                    'metadata': metadata
                })
                if kind == 'Service':
                    resource['spec']['clusterIP'] = old_resource['spec']['clusterIP']
                response = self.update_resource(uri=uri, body=resource, name=name)
                if response.status_code in [200, 201]:
                    logging.info("Resource {0}/{1} patched SUCCESSFULLY".format(kind, name))
                else:
                    logging.info("FAILED to patch resource {0}/{1}: {2}".format(kind, name, response.text))
                    failed_resources.append((kind, name))
            else:
                response = self.create_resource(uri=uri, body=resource)
                if response.status_code in [200, 201]:
                    logging.info("Resource {0}/{1} created SUCCESSFULLY".format(kind, name))
                else:
                    logging.info("FAILED to create resource {0}/{1}: {2}".format(kind, name, response.text))
                    failed_resources.append((kind, name))
        if len(failed_resources) > 0:
            logging.info("Resources failed to patch/create: {0}".format(", ".join(failed_resources)))
        return len(failed_resources) == 0

    def delete_resources2(self, **kwargs):
        resources = self.process_template(**kwargs)['items']
        logging.debug("Processing resources: {0}".format(resources))
        failed_resources = []
        api_prefix = lambda kind: [apiver for apiver, kinds in self.api_prefixes if kind in kinds][0]
        failed_resources = []
        for resource in resources:
            kind = resource['kind']
            name = resource['metadata']['name']
            api_version = resource['apiVersion']
            uri='/{api_prefix}/{api_version}/namespaces/{project}/{kind}s'.format(
                api_prefix=api_prefix(kind), api_version=api_version, kind=kind.lower(),
                project=self.p.project)
            response = self.delete_resource(uri=uri, name=name)
            if response.status_code in [200, 201]:
                logging.info("Request to DELETE resource {0}/{1} SUCCESSFULLY sent".format(kind, name))
            elif response.status_code in [404,]:
                logging.info("Resource {0}/{1} does NOT EXIST".format(kind, name))
            else:
                logging.info("Request to DELETE resource {0}/{1} FAILED".format(kind, name))
                failed_resources.append((kind, name))

        if len(failed_resources) > 0:
            logging.info("Resources failed to DELETE: {0}".format(failed_resources))
        return len(failed_resources) == 0

    def create_resource(self, **kwargs):
        validate_params(
            required_params=('uri', 'body'),
            **kwargs
        )
        args = copy.deepcopy(kwargs)
        args.update({
            'method': 'POST',
            'body': kwargs['body']
        })
        return self.make_call(**args)

    def update_resource(self, **kwargs):
        validate_params(
            required_params=('uri', 'name', 'body'),
            **kwargs
        )
        args = copy.deepcopy(kwargs)
        args.update({
            'method': 'PUT',
            'body': kwargs['body']
        })
        return self.make_call(**args)

    def delete_resource(self, **kwargs):
        validate_params(
            required_params=('uri', 'name'),
            optional_params=('body',),
            **kwargs
        )
        args = copy.deepcopy(kwargs)
        args.update({
            'method': 'DELETE'
        })
        return self.make_call(**args)

    def status_resource(self, **kwargs):
        validate_params(
            required_params=('uri', 'name'),
            **kwargs
        )
        args = copy.deepcopy(kwargs)
        args.update({
            'method': 'GET'
        })
        return self.make_call(**args)


    def process_template(self, **kwargs):
        """
        Requires params:
            - template
            - re_mask a re compile expression to search variables
            - params is a dict with variables to substitue
        """
        def __process_template(**kwargs):
            """substitute variables to values"""
            template = kwargs['template']
            re_mask = kwargs.get('re_mask', None)
            if re_mask is None:
                re_mask = re.compile(r'\$\{(\w+)\}+')

            value = template
            if isinstance(value, str):
                found_vars = re_mask.findall(value)
                if len(found_vars) > 0:
                    for key in found_vars:
                        value = value.replace('${' + key + '}', kwargs['params'][key])
            elif isinstance(value, dict):
                for key in value.keys():
                    value[key] = __process_template(template=value[key], re_mask=re_mask, params=kwargs['params'])
            elif isinstance(value, list):
                new_values = []
                for key in value:
                    new_values.append(__process_template(template=key, re_mask=re_mask, params=kwargs['params']))
                value = new_values
            template = value
            return template


        template = populate_ports(
            template=kwargs['template'],
            services=self.p.services,
            env_vars=self.p.env_vars,
            attach_certs=self.p.attach_certs,
            attach_files=self.p.attach_files)
        assert(isinstance(template, dict)), "Template must be a valid Dict object"
        processed_template = __process_template(template=template, params={
            'APP_NAME': self.p.app_name,
            'IMAGE_NAME': self.p.image_name
        })
        return processed_template


    def make_call(self, **kwargs):
        validate_params(
            required_params=('uri', 'method'),
            optional_params=('headers', 'http_proxy', 'https_proxy', 'timeout', 'body', 'resource_name', 'name'),
            **kwargs
        )
        assert(kwargs['method'] in ['POST', 'GET', 'PUT', 'DELETE']), "HTTP method migth be one of POST, GET, PUT, DELETE"

        headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
        self.__session.headers.update(kwargs.get('headers', headers))
        url = '{0}{1}{2}'.format(self.p.server, kwargs['uri'], '/' + kwargs['name'] if 'name' in kwargs.keys() else '')

        logging.debug("Calling {1} at {0}".format(kwargs['uri'], kwargs['method']))

        response = self.__session.request(method=kwargs['method'], url=url, verify=True,
            proxies={
                'http_proxy': self.p.http_proxy if hasattr(self.p, 'http_proxy') else '',
                'https_proxy': self.p.https_proxy if hasattr(self.p, 'https_proxy') else '',
            },
            timeout=self.p.timeout if hasattr(self.p, 'timeout') else 60,
            json=kwargs.get('body', {}),
        )
        logging.debug('Request: {0}, returned code: {1}'.format(url, response.status_code))
        return response




    def login(self):
        """checks whether it token is valid"""
        response = self.make_call(method='GET', uri='/oapi/v1')
        if response.status_code not in [200, 201]:
            logging.info("Cannot log in to OpenShift: {0}".format(response.text))
        else:
            logging.info("Provided token is valid")
        return response.status_code in [200, 201]

    def create_project(self):
        response = self.make_call(method='GET', uri='/apis/project.openshift.io/v1/projects')
        if response.status_code in [200, ]:
            if any([project['metadata']['name'] for project in response.json()['items'] if project['metadata']['name'] == self.p.project]):
                logging.info("Project '{0}' exists".format(self.p.project))
            else:
                logging.debug("Project '{0}' does not exist".format(self.p.project))
                body = {
                    'version': 'project.openshift.io/v1',
                    'kind': 'ProjectRequest',
                    'metadata': {
                        'name': self.p.project
                    }
                }
                headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
                response = self.make_call(method='POST', uri='/apis/project.openshift.io/v1/projectrequests', body=body, headers=headers)
                if response.status_code in [200, 201]:
                    timeout = 300
                    while response.json()['status']['phase'] != 'Active' and timeout > 0:
                        logging.debug("Project is not in the Active state. Wait for next 7 seconds...")
                        timeout -= 7
                        sleep(7)
                        response = self.make_call(method='GET', uri='/apis/project.openshift.io/v1/projects/{0}'.format(self.p.project))
                    else:
                        logging.info("Project '{0}' created SUCCESSFULLY".format(self.p.project))
                        return True
                else:
                    logging.error("Could not create project '{0}': {1}".format(self.p.project, response.text))
                    return False
        elif response.status_code in [409,] and response.json()['code'] in [409,]:
            logging.info("Project '{0}' created SUCCESSFULLY".format(self.p.project))
        else:
            logging.error("Could not get a list of existing projects: {0}".format(response.text))
            return False
        return True


    def delete_project(self):
        response = self.make_call(method='DELETE', uri='/apis/project.openshift.io/v1/projects/{0}'.format(self.p.project))
        if response.status_code in [200, 201]:
            logging.info("Request to DELETE project '{0}' sent successfully".format(self.p.project))
            return True
        else:
            logging.error("Request to DELETE project '{0}' wasn't successful: {1}".format(self.p.project, response.text))
            return False

    def list_projects(self):
        response = self.make_call(method='GET', uri='/apis/project.openshift.io/v1/projects'.format(self.p.project))
        return [proj['metadata']['name'] for proj in response.json()['items']]

    def build_all(self):
        return  self.login() and \
                self.create_project() and \
                self.install_resources(template=TEMPLATE)

    def delete_all(self):
        delete_resources = lambda:  self.delete_resources2(template=TEMPLATE)
        project_exists   = lambda:  self.p.project in self.list_projects()
        delete_project   = lambda:  self.delete_project() if self.p.delete_project else True
        return  self.login() and \
                (not project_exists() or (project_exists() and delete_resources())) and \
                delete_project()


def main():
    parser = argparse.ArgumentParser(description='Build project')
    parser.add_argument('-a', '--action', choices=('install', 'cleanup', 'devel'), help='Action to perform')
    parser.add_argument('-s', '--server', help="OpenShift API endpoint")
    parser.add_argument('-p', '--project', help="OpenShift project")
    parser.add_argument('-n', '--app-name', help="Name for OpenShift deployment and associated resources")
    parser.add_argument('-i', '--image-name', default='', help="Image name to deploy")
    parser.add_argument('--services', default='[]', help='Services to expose. Ex. \'[{"port":"8443","hostname":"myapp.com"},{"port":"8444"}]\'')
    parser.add_argument('-t', '--token', help="OpenShift bearer token")
    parser.add_argument('--env-vars', default='{}', help='Environment variables. Ex. \'{"CERT_PATH":"/certs","PRIVATE_KEY":"private.key"}\'')
    parser.add_argument('--attach-certs', default='{}', help='Files to attach as a secret. Ex. \'[{"mount_to":"/certs","files":["private.key","server.crt"},{"mount_to":"/configs",files:["nginx.conf","site.conf"}]}\'')
    parser.add_argument('--attach-files', default='[]', help='Files to attach as config maps. Ex. \'[{"mount_to":"/certs","files":["private.key","server.crt"},{"mount_to":"/configs",files:["nginx.conf","site.conf"}]}\'')
    parser.add_argument('--timeout', default=60, help="Timeout for each request")
    parser.add_argument('--http-proxy', default='', help="Proxy for http connecions")
    parser.add_argument('--https-proxy', default='', help="Proxy for https connecions")
    parser.add_argument('--log-level', default='debug', choices=('info', 'debug', 'warning', 'error', 'critical'),  help="Logs verbosity (default: debug)")
    parser.add_argument('--delete-project', action='store_true', help='Delete project upon "cleanup" action')

    args = parser.parse_args()

    if not args.server.startswith('https://'):
        raise parser.error("Parameter --server should start with schema https://")

    log_levels = {'info': logging.INFO, 'debug': logging.DEBUG, 'warning': logging.WARNING, 'error': logging.ERROR, 'critical': logging.CRITICAL}
    logging.getLogger().setLevel(log_levels[args.log_level])

    kwargs = {}
    kwargs.update(args.__dict__)
    kwargs.update({
        'services': json.loads(args.services),
        'env_vars': json.loads(args.env_vars),
        'attach_certs': json.loads(args.attach_certs),
        'attach_files': json.loads(args.attach_certs),
    })
    kwargs.pop('action')
    kwargs.pop('log_level')
    #for arg in args.__dict__.keys():
    #    kwargs[arg] = args[arg]

    c = RESTClient(**kwargs)
    if args.action in ['devel','install']:
        if kwargs.get('image_name', '') == '':
            parser.error("Parameter --image-name must be given")
        c.build_all()
    elif args.action == 'cleanup':
        c.delete_all()
    pass


if __name__ == "__main__":
    main()
